from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

import datetime

# Create your models here.


class Post(models.Model):
    post_author = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField("Date of publication")

    post_title = models.CharField(max_length=2000)
    post_subtitle = models.CharField(max_length=2000, default="Post subtitle")
    post_body = models.TextField(default="Post title")

    post_image = models.CharField(
        max_length=2000)

    def __str__(self):
        return f"'{self.post_title}' - {self.post_author.username.title()}"

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
