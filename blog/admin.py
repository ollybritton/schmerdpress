from django.contrib import admin

from .models import Post

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Post Content", {
            "fields": ["post_title", "post_subtitle", "post_body"]
        }),
        ("Display Image", {
            "fields": ["post_image"]
        }),
        ("Metadata", {
            "fields": ["post_author", "pub_date"]
        })
    ]


admin.site.register(Post, PostAdmin)
