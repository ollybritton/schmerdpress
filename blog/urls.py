from django.urls import path, include
from . import views

app_name = "blog"
urlpatterns = [
    path('', views.PostListView.as_view(), name="index"),
    path('posts/', views.PostAllListView.as_view(), name="post-list"),
    path('post/<int:pk>/', views.PostView.as_view(), name="post")
]
