from django.urls import reverse
from django.http import HttpResponseRedirect
from django.views import generic
from django.shortcuts import render

from .models import Post

# Create your views here.


class PostView(generic.DetailView):
    model = Post
    template_name = "blog/post.html"


class PostListView(generic.ListView):
    template_name = "blog/index.html"
    context_object_name = "posts"

    def get_queryset(self):
        return Post.objects.order_by("-pub_date")[:5]


class PostAllListView(generic.ListView):
    template_name = "blog/post_list.html"
    context_object_name = "posts"

    def get_queryset(self):
        return Post.objects.order_by("-pub_date")
