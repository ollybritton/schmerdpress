from django.contrib import admin
from .models import Page

# Register your models here.


class PageAdmin(admin.ModelAdmin):
    fieldsets = [
        ("URL", {
            "fields": ["page_pattern"]
        }),
        ("Title", {
            "fields": ["page_title", "page_subtitle", "page_image"]
        }),
        ("Content", {
            "fields": ["page_content"]
        })
    ]


admin.site.register(Page, PageAdmin)
