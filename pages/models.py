from django.db import models

# Create your models here.


class Page(models.Model):
    page_pattern = models.CharField(max_length=200)

    page_title = models.CharField(max_length=200)
    page_subtitle = models.CharField(max_length=2000)

    page_image = models.CharField(max_length=2000)
    page_content = models.TextField()

    def __str__(self):
        return "{} | {}".format(self.page_title, self.page_pattern)
