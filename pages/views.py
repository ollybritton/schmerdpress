from django.shortcuts import render
from django.views import generic
from .models import Page

# Create your views here.


class PageView(generic.DetailView):
    model = Page
    template_name = "pages/page_view.html"
    slug_field = "page_pattern"
    slug_url_kwarg = "page_pattern"
