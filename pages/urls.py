from django.urls import path, include
from django.views.generic import TemplateView
from . import views

app_name = "pages"
urlpatterns = [
    path("<slug:page_pattern>/",
         views.PageView.as_view(), name="page-detail")
]
