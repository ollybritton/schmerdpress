# Schmerdpress
Schmerdpress is an application I am building to help develop my skills using the Django framework. It acts as a very stripped-down version of Wordpress (hence the name). This document is just for planning the application structure/architecture.

## Sections
The project has two main parts, viewing and editing.

## Viewing
Viewing is the side of the project which is what you'd expect to see when you access a typical Wordpress website, where you can:

* View the recent posts
* Look at all the posts
* Search/filter the posts.
* View pages that are not part of the blog, i.e. "Contact" or "Privacy Policy".

The blog itself is going to be implemented in the **Blog** app, and all the other fancy pages such as *contact* in the **Pages** app.

## Editing
This is what the typical admin of the site is faced with: editing posts, creating new pages and more. This is not actually going to be implemented by me, and instead is going to be handled using Django's built in admin application where we can add our own options in the `admin.py` file.